# End Murder

This repo contains the work in progress for the list of murder sites known to be holding individuals against their will.

## Submissions

When submitting changes, make sure the spirit of the request for help is intact, and the information found about the facility is publicly accessible. Then, make sure the single file nature of the entreatment itself is kept intact. We need all of the facilities to be addressed in a single entry, so refreshing and transfer won't be interrupted on loss of cohesion because of multiple file blocks on some networks.

Fork from https://gitlab.com/endmurder/endmurder, push a branch with your commit adding/updating with information about where the update is about in the commit message, then open a merge request at https://gitlab.com/endmurder/endmurder/-/merge_requests/new. The merge request is where the confirmation will be discussed. If the update is verified reachable, or the translation correct, the merge request should be accepted if it does not violate the single entry requirement of the entreatment file.
