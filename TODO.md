# TODO

* Add submission form
* Add notification on-change sign up
* Website
* Inversion testing for possible sites
  * This needs to have a way to know we've covered every possible area, even places that don't publicly report murder facilities
  * For example, a list of all known countries, and explanation of how some of them are proven to not have impossible to escape areas
* Add coordination of efforts
* (?) Make an app
  * Warning of impending police brutality zone
    * If you're in danger of an oppressive regime arresting you just for being alive, you'd get notification
